package com.company;

public class Main {

    public static void main(String[] args)
    {
        Integer i = new Integer(1);
        Integer num = new Integer(0);
        num = ++i ;
        System.out.println(num);
        num = --i ;
        System.out.println(num);
        num = i++ ;
        System.out.println(num);
        num = i-- ;
        System.out.println(num);
    }
}
